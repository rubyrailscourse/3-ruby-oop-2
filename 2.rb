# Расширьте класс Point
# Добавьте метод Point#similar_quarter?
class Point
  attr_accessor :x, :y
  def initialize x, y
    @x = x
    @y = y
  end

  def similar_quarter?(point)
    self.x * point.x > 0 && self.y * point.y > 0 ? true : false
  end

  def origin?
    @x == 0 && @y == 0 ? true : false
  end

  def to_s
    puts "x = #{@x}, y = #{@y}"
  end

  def set_origin
    @x = 0
    @y = 0
  end
end

point0 = Point.new(0, 0)
point1 = Point.new(2, 7)
point2 = Point.new(-3, 5)
point3 = Point.new(4, 2)

puts point1.similar_quarter?(point2)
puts point1.similar_quarter?(point3)
puts point0.origin?
puts point2.origin?
point2.set_origin
puts point2
