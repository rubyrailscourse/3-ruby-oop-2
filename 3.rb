# Напишите собственный класс
class Kettle
  attr_accessor :max_volume, :water_volume, :color, :power, :last_boil

  def initialize (max_volume, power, color)
    @max_volume = max_volume
    @power = power
    @color = color
    @water_volume = 0
    @last_boil = Time.new(0)
  end

  def to_s
    puts "Kettle water volume: #{@water_volume} l, max volume: #{@max_volume} l, last boil: #{@last_boil}, color: #{@color}, power: #{@power} watts"
  end

  def fill
    @water_volume = @max_volume
    @last_boil = Time.new(0)
    puts "Kettle filled!"
  end

  def boil
    puts result = if @water_volume > 0.3
      @last_boil = Time.now + 300
      "Boiled water!"
    else
      "No water!"
    end
  end

  def pour_cup
    puts result = if Time.now > @last_boil + 3600
      "You must to boil water"
    elsif @water_volume > 0.2
      @water_volume -= 0.2
      "One cup filled!"
    else
      "No water!"
    end
  end
end
